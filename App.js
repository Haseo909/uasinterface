import React, { Component } from 'react';
import {
  Alert, 
  AppRegistry, 
  StyleSheet, 
  View, 
  Text, 
  TextInput,
  ScrollView,
  Touchable, 
  Button
} 
from 'react-native';


export default class App extends Component{
  constructor(props){
    super(props);
    this.state = {
        noteArray: [],
        noteText: '',
    };
}


  render() {
    let notes = this.state.noteArray.map((val, key)=>{
      return <Note key={key} keyval={key} val={val}
              deleteMethod={()=>this.deleteNote(key)}/>
  });
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}> TO DO APP </Text>

        <View style={styles.buttonContainer}>
          <Button
            onPress={this._onPressButton}
            title="Klik"
          />
        </View>
        <TextInput
          style={{height: 40}}
          placeholder="Tulis Aktivitas Anda!"
          onChangeText={(text) => this.setState({text})}
        />
       
       
        <View style={styles.alternativeLayoutButtonContainer}>
          <Button
            onPress={this._onPressButton}
            title="This looks great!"
          />
          <Button
            onPress={this._onPressButton}
            title="OK!"
            color="#841584"
          />
        </View>
      </View>
    
        
    );
   }
}


  
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },buttonContainer: {
    margin: 20
  },
  alternativeLayoutButtonContainer: {
    margin: 20,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
});

AppRegistry.registerComponent('AwesomeProject', () => ButtonBasics);
